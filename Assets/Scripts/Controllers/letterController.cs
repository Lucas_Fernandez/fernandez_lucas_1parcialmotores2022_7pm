using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class letterController : MonoBehaviour
{
    public bool isActive;

    void Start()
    {
        //Initialize
        isActive = true;
    }

    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        //OnTrigger with Player sets active to false and selfDeactivates
        if (other.gameObject.CompareTag("Player") == true)
        {
            isActive = false;
            gameObject.SetActive(false);
        }
    }

    //Activates (use for another script)
    public void activate()
    {
        gameObject.SetActive(true);
    }

    //Deactivates (use for another script)
    public void deActivate()
    {
        gameObject.SetActive(false);
    }
}
