using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minionScript : MonoBehaviour
{
    void Start()
    {
    }

    void Update()
    {
    }

    private void OnCollisionEnter(Collision col)
    {
        //OnCollision with projectile or player it destroys
        if (
            col.gameObject.CompareTag("Projectile") ||
            col.gameObject.CompareTag("Player")
        )
        {
            Destroy (gameObject);
        }
    }
}
