using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour
{
    //Same as used in fps lesson

    Vector2 mouseLook;
    Vector2 smoothnessV;

    public float sensitivity = 5.0f;
    public float softness = 2.0f;

    GameObject player;

    void Start()
    {
        player = this.transform.parent.gameObject;
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensitivity * softness, sensitivity * softness));

        smoothnessV.x = Mathf.Lerp(smoothnessV.x, md.x, 1f / softness);
        smoothnessV.y = Mathf.Lerp(smoothnessV.y, md.y, 1f / softness);

        mouseLook += smoothnessV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, player.transform.up);
    }
}