using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonController : MonoBehaviour
{
    public bool pushed;

    void Start()
    {
        //Initialize
        pushed = false;
    }

    //To be used from another script, sets pushed to true
    public void activate()
    {
        pushed = true;
    }

    //To be used from another script, returns pushed
    public bool isPushed()
    {
        return pushed;
    }
}
