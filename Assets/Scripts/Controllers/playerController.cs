using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    public int hp;

    private Rigidbody rb;

    public float movementSpeed = 10.0f;

    public Camera firstPersonCamera;

    public LayerMask floor;

    private bool doubleJump;

    public bool needsToDebuff;

    public float jumpMagnitude;

    public float doubleJumpMagnitude;

    public GameObject gun;

    public GameObject projectile;

    public CapsuleCollider col;

    float boosterTime;

    float timeToHide;

    void Start()
    {
        //Initialize
        hp = 100;
        needsToDebuff = false;
        Cursor.lockState = CursorLockMode.Locked;

        //Getters
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
    }

    void Update()
    {
        baseControllers();
        shootingHandler();
        setDoubleJump();
        jumpController();
    }

    //Same as implemented in fps lesson
    private void baseControllers()
    {
        float backAndFrontMovement = Input.GetAxis("Vertical") * movementSpeed;
        float leftAndRightMovement =
            Input.GetAxis("Horizontal") * movementSpeed;

        backAndFrontMovement *= Time.deltaTime;
        leftAndRightMovement *= Time.deltaTime;

        transform.Translate(leftAndRightMovement, 0, backAndFrontMovement);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    //Same as implemented in fps lesson with some tweaks
    private void shootingHandler()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Raycasting
            Ray ray =
                firstPersonCamera
                    .ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            //Display hitted object
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 0.5f)
            {
                Debug.Log("Ray has hit " + hit.collider.name);
            }

            buttonHittingHandler (hit);
            handleChamberFourButtons (hit);
            projectileShooting();
        }
    }

    //When hitting a button it sets the button activeValue to true
    private void buttonHittingHandler(RaycastHit hit)
    {
        if (hit.collider.name.Substring(0) == "Button")
        {
            GameObject hittedObject = GameObject.Find(hit.transform.name);
            buttonController scripthittedObject =
                (buttonController)
                hittedObject.GetComponent(typeof (buttonController));
            if (scripthittedObject != null)
            {
                scripthittedObject.activate();
            }
        }
    }

    //Manages the fourth chamber buttons
    void handleChamberFourButtons(RaycastHit hit)
    {
        //Checks for the hitted button and activates the corresponding ramp, should be more generic
        if (hit.collider.name.Substring(0) == "Chamber4Button1")
        {
            GameObject ramp = GameObject.Find("Chamber4Ramp1");
            showItem (ramp);
            StartCoroutine(waitToHideItem(2, ramp));
        }

        if (hit.collider.name.Substring(0) == "Chamber4Button2")
        {
            GameObject ramp = GameObject.Find("Chamber4Ramp2");
            showItem (ramp);
            StartCoroutine(waitToHideItem(2, ramp));
        }

        if (hit.collider.name.Substring(0) == "Chamber4Button3")
        {
            GameObject ramp = GameObject.Find("Chamber4Ramp3");
            showItem (ramp);
            StartCoroutine(waitToHideItem(2, ramp));
        }
    }

    //Completely hides an item using meshRenderer and Collider handler
    private void hideItem(GameObject item)
    {
        if (item != null)
        {
            meshRendererHandler meshScript =
                (meshRendererHandler)
                item.GetComponent(typeof (meshRendererHandler));

            colliderHandler colScript =
                (colliderHandler) item.GetComponent(typeof (colliderHandler));
            if (meshScript != null && colScript != null)
            {
                meshScript.disableMesh();
                colScript.disableColilider();
            }
        }
    }

    //Completely shows an item using meshRenderer and Collider handler
    private void showItem(GameObject item)
    {
        if (item != null)
        {
            meshRendererHandler meshScript =
                (meshRendererHandler)
                item.GetComponent(typeof (meshRendererHandler));

            colliderHandler colScript =
                (colliderHandler) item.GetComponent(typeof (colliderHandler));
            if (meshScript != null && colScript != null)
            {
                meshScript.enableMesh();
                colScript.enableColilider();
            }
        }
    }

    //Hides an item in certain time period
    public IEnumerator waitToHideItem(float time, GameObject item)
    {
        timeToHide = time;
        while (timeToHide > 0)
        {
            yield return new WaitForSeconds(1.0f);
            timeToHide--;
        }
        hideItem (item);
    }

    void projectileShooting()
    {
        /*Uses the meshRendererHandler script to know 
            if the player is able to shoot projectiles, 
                if the gun's meshRend is enabled it can shoot*/
        if (gun != null)
        {
            meshRendererHandler script =
                (meshRendererHandler)
                gun.GetComponent(typeof (meshRendererHandler));

            if (script.rendererEnabled())
            {
                shootProjectile();
            }
        }
    }

    //Same as implemented in fps lesson
    void shootProjectile()
    {
        Ray rayPr =
            firstPersonCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        GameObject pro;
        pro = Instantiate(projectile, rayPr.origin, transform.rotation);

        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb
            .AddForce(firstPersonCamera.transform.forward * 15,
            ForceMode.Impulse);

        Destroy(pro, 5);
    }

    //If the player is on floorLayer, doubleJump is set to true
    private void setDoubleJump()
    {
        if (onFloor())
        {
            doubleJump = true;
        }
    }

    //Returns if player is onFloor layer;
    private bool onFloor()
    {
        return Physics
            .CheckCapsule(col.bounds.center,
            new Vector3(col.bounds.center.x,
                col.bounds.min.y,
                col.bounds.center.z),
            col.radius * .9f,
            floor);
    }

    private void jumpController()
    {
        /*If pressing space and player is onFloor layer, 
            player jumps and doubleJump is set to true*/
        if (Input.GetKeyDown(KeyCode.Space) && onFloor())
        {
            Debug.Log("Jumping");
            rb.AddForce(Vector3.up * jumpMagnitude, ForceMode.Impulse);
            doubleJump = true;
        }
        else /*If pressing space and doubleJump is true, 
            player is allowed to perform another jump
                then doubleJump is set to false*/
        if (Input.GetKeyDown(KeyCode.Space) && doubleJump)
        {
            Debug.Log("Double Jumping");
            rb.AddForce(Vector3.up * doubleJumpMagnitude, ForceMode.Impulse);
            doubleJump = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //OnTrigger with Booster changes scale and speed of player
        if (other.gameObject.CompareTag("Booster"))
        {
            col.gameObject.transform.localScale =
                new Vector3(0.45f, 0.6f, 0.6f);
            movementSpeed = 4.5f;

            //Hide Booster
            other.gameObject.SetActive(false);

            //Starts countdown for booster duration
            StartCoroutine(BoosterLimit(6));
        }

        //OnTrigger with Gun, it enables the gun
        if (other.gameObject.CompareTag("Gun"))
        {
            //Hide Gun
            other.gameObject.SetActive(false);
            enableGun();
        }
    }

    //Booster countdown
    public IEnumerator BoosterLimit(float seconds)
    {
        boosterTime = seconds;
        while (boosterTime > 0)
        {
            Debug.Log("Booster has " + boosterTime + "s left");
            yield return new WaitForSeconds(1.0f);
            boosterTime--;
        }
        deBuff();
    }

    //Normilizes player's speed and scale
    private void deBuff()
    {
        col.gameObject.transform.localScale = new Vector3(0.15f, 0.19f, 0.2f);
        movementSpeed = 2f;
    }

    //Enables the meshRend of the player's Gun using the meshRendererHandler
    public void enableGun()
    {
        if (gun != null)
        {
            meshRendererHandler script =
                (meshRendererHandler)
                gun.GetComponent(typeof (meshRendererHandler));

            if (script != null)
            {
                script.enableMesh();
            }
        }
    }

    //Disables the meshRend of the player's Gun using the meshRendererHandler
    public void disableGun()
    {
        if (gun != null)
        {
            meshRendererHandler script =
                (meshRendererHandler)
                gun.GetComponent(typeof (meshRendererHandler));

            if (script != null)
            {
                script.disableMesh();
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        //All Collisions make damage
        if (col.gameObject.CompareTag("Barrel"))
        {
            takeDamage(10);
        }

        if (col.gameObject.CompareTag("BossThreeChild"))
        {
            takeDamage(10);
        }

        if (col.gameObject.CompareTag("BossFour"))
        {
            takeDamage(35);
        }

        if (col.gameObject.CompareTag("BossFive"))
        {
            takeDamage(100);
        }
    }

    private void takeDamage(int damage)
    {
        hp -= damage;
    }

    //Hp getter for another script
    public int getHp()
    {
        return hp;
    }
}
