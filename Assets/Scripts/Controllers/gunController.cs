using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunController : MonoBehaviour
{
    public bool activeness;

    void Start()
    {
        activeness = true;
    }

    void Update()
    {
    }

    //Sets bool to false, for use in another script
    public void deactivate(){
        activeness = false;
    }
}
