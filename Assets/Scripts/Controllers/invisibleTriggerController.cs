using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class invisibleTriggerController : MonoBehaviour
{
    public bool triggered;

    public bool bossLeftArea;

    //Used to autoreferentiate
    public GameObject trigger;

    private BoxCollider col;

    void Start()
    {
        //Initialize
        triggered = false;
        bossLeftArea = false;

        //Getter
        col = trigger.GetComponent<BoxCollider>();
    }

    void Update()
    {
    }

    private bool OnTriggerEnter(Collider other)
    {
        //OnTrigger with Player returns true, else ruturns false (bool triggered value)
        if (other.gameObject.CompareTag("Player") == true)
        {
            triggered = true;
        }
        return triggered;
    }

    private void OnTriggerExit(Collider other)
    {
        //OnExit with Boss Four it sets the bossLeftArea to true (Should be more generic)
        if (other.gameObject.CompareTag("BossFour"))
        {
            bossLeftArea = true;
        }
    }

    //Self destructs, Destroy(gameObject) sometimes crashes
    public void Destroy()
    {
        Destroy (trigger);
    }
}
