using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnableDoorController : MonoBehaviour
{
    Collider col;

    public MeshRenderer rend;

    void Start()
    {
        //Getters
        col = GetComponent<Collider>();
        rend = GetComponent<MeshRenderer>();
    }

    void Update()
    {
    }

    /*2 methods to enable and disable the mesh and collider of the door, 
        this was later implemented as two separate scripts*/

    public void enableColliderAndMesh()
    {
        col.enabled = true;
        rend.enabled = true;
    }

    public void disableColliderAndMesh()
    {
        col.enabled = false;
        rend.enabled = false;
    }
}
