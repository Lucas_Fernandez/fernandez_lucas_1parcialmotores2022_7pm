using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class fifthChamberManager : MonoBehaviour
{
    private bool playerOnChamber;

    public GameObject doorTrigger;

    public GameObject player;

    private int playerHp;

    public GameObject boss;

    public GameObject gun;

    public GameObject doorToSpawn;

    public List<GameObject> chamberObjects = new List<GameObject>();

    public GameObject letter;

    public TMPro.TMP_Text remainingTimeText;

    public TMPro.TMP_Text gameOverText;

    public TMPro.TMP_Text hpLeftText;

    float timeLeft;

    void Start()
    {
        playerOnChamber = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (doorTrigger != null)
        {
            invisibleTriggerController script =
                (invisibleTriggerController)
                doorTrigger.GetComponent(typeof (invisibleTriggerController));

            if (script != null)
            {
                if (script.triggered)
                {
                    playerOnChamber = true;
                    script.Destroy();
                    SetHpText();
                    StartCoroutine(TimeLeft(60));
                    StartChamber();
                }
            }
        }

        if (doorTrigger == null)
        {
            SetHpText();
        }

        if (playerOnChamber)
        {
            if (
                GameObject
                    .FindGameObjectWithTag("BossFive")
                    .transform
                    .position ==
                new Vector3(-100, -100, -100)
            )
            {
                EndChamber();
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && playerOnChamber)
        {
            StartChamber();
        }
    }

    void StartChamber()
    {
        hideLetter (letter);
        spawnDoor (doorToSpawn);
        destroyChamberObjects();
        player.transform.position =
            new Vector3(-4.44399834f, 0.575802028f, 8.49399948f);
        chamberObjects
            .Add(Instantiate(gun,
            new Vector3(-10.1639996f, 0.791999996f, 13.099f),
            Quaternion.identity));
        instantiateEnemies();
    }

    void SetHpText()
    {
        if (player != null)
        {
            playerController script =
                (playerController)
                player.GetComponent(typeof (playerController));
            if (script != null)
            {
                playerHp = script.getHp();
            }
        }

        hpLeftText.text = playerHp.ToString() + "hp left";
        if (playerHp <= 0)
        {
            gameOverText.text = "GAME OVER";

            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }

    public IEnumerator TimeLeft(float time)
    {
        timeLeft = time;
        while (timeLeft > -1)
        {
            remainingTimeText.text = timeLeft + "s left";
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
        gameOverText.text = "GAME OVER";

        yield return new WaitForSeconds(.4f);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    void instantiateEnemies()
    {
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(-19.8099995f, 3.6099999f, 19.9599991f),
            Quaternion.Euler(0, 307.069f, 0)));
    }

    void spawnDoor(GameObject door)
    {
        if (door != null)
        {
            spawnableDoorController script =
                (spawnableDoorController)
                door.GetComponent(typeof (spawnableDoorController));

            if (script != null)
            {
                script.enableColliderAndMesh();
            }
        }
    }

    void deSpawnDoor(GameObject door)
    {
        if (door != null)
        {
            spawnableDoorController script =
                (spawnableDoorController)
                door.GetComponent(typeof (spawnableDoorController));

            if (script != null)
            {
                script.disableColliderAndMesh();
            }
        }
    }

    void destroyChamberObjects()
    {
        foreach (GameObject item in chamberObjects)
        {
            Destroy (item);
        }
    }

    void revealLetter(GameObject letter)
    {
        if (letter != null)
        {
            letterController script =
                (letterController)
                letter.GetComponent(typeof (letterController));
            if (script != null)
            {
                script.activate();
            }
        }
    }

    void hideLetter(GameObject letter)
    {
        if (letter != null)
        {
            letterController script =
                (letterController)
                letter.GetComponent(typeof (letterController));
            if (script != null)
            {
                script.deActivate();
            }
        }
    }

    void disablePlayerGun()
    {
        if (player != null)
        {
            playerController script =
                (playerController)
                player.GetComponent(typeof (playerController));
            if (script != null)
            {
                script.disableGun();
            }
        }
    }

    void EndChamber()
    {
        destroyChamberObjects();
        disablePlayerGun();
        revealLetter (letter);
        deSpawnDoor (doorToSpawn);
        Destroy (remainingTimeText);
        Destroy (hpLeftText);
        Destroy (gameObject);
    }
}
