using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    public GameObject letterM;

    public TMPro.TMP_Text M;

    public GameObject letterFirstO;

    public TMPro.TMP_Text firstO;

    public GameObject letterT;

    public TMPro.TMP_Text T;

    public GameObject letterSecondO;

    public TMPro.TMP_Text secondO;

    public GameObject letterR;

    public TMPro.TMP_Text R;

    public TMPro.TMP_Text win;

    public TMPro.TMP_Text gameOver;

    public GameObject globalDeathVoid;

    private float timeLeft;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        checkLetterM();
        checkLetterFirstO();
        checkLetterT();
        checkLetterSecondO();
        checkLetterR();

        if (globalDeathVoid != null)
        {
            invisibleTriggerController script =
                (invisibleTriggerController)
                globalDeathVoid
                    .GetComponent(typeof (invisibleTriggerController));

            if (script != null)
            {
                if (script.triggered)
                {
                    gameOver.text = "GAME OVER";
                    StartCoroutine(resettingIn(1));
                }
            }
        }

        if (
            M.text != "" &&
            firstO.text != "" &&
            T.text != "" &&
            secondO.text != "" &&
            R.text != ""
        )
        {
            win.text = "YOU WIN";
            StartCoroutine(resettingIn(3));
        }
    }

    void checkLetterM()
    {
        if (letterM != null)
        {
            letterController script =
                (letterController)
                letterM.GetComponent(typeof (letterController));
            if (!script.isActive)
            {
                M.text = "M";
            }
        }
    }

    void checkLetterFirstO()
    {
        if (letterFirstO != null)
        {
            letterController script =
                (letterController)
                letterFirstO.GetComponent(typeof (letterController));
            if (!script.isActive)
            {
                firstO.text = "O";
            }
        }
    }

    void checkLetterT()
    {
        if (letterT != null)
        {
            letterController script =
                (letterController)
                letterT.GetComponent(typeof (letterController));
            if (!script.isActive)
            {
                T.text = "T";
            }
        }
    }

    void checkLetterSecondO()
    {
        if (letterSecondO != null)
        {
            letterController script =
                (letterController)
                letterSecondO.GetComponent(typeof (letterController));
            if (!script.isActive)
            {
                secondO.text = "O";
            }
        }
    }

    void checkLetterR()
    {
        if (letterR != null)
        {
            letterController script =
                (letterController)
                letterR.GetComponent(typeof (letterController));
            if (!script.isActive)
            {
                R.text = "R";
            }
        }
    }

    public IEnumerator resettingIn(float time)
    {
        timeLeft = time;
        while (timeLeft > -1)
        {
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }

        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
