using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class fourthChamberManager : MonoBehaviour
{
    private bool playerOnChamber;

    public GameObject doorTrigger;

    public GameObject player;

    private int playerHp;

    public GameObject boss;

    public GameObject doorToSpawn;

    public GameObject activeArea;

    public GameObject nextDoorChamber;

    public List<GameObject> chamberObjects = new List<GameObject>();

    public GameObject letter;

    public TMPro.TMP_Text remainingTimeText;

    public TMPro.TMP_Text gameOverText;

    public TMPro.TMP_Text hpLeftText;

    float timeLeft;

    void Start()
    {
        playerOnChamber = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (doorTrigger != null)
        {
            invisibleTriggerController script =
                (invisibleTriggerController)
                doorTrigger.GetComponent(typeof (invisibleTriggerController));

            if (script != null)
            {
                if (script.triggered)
                {
                    playerOnChamber = true;
                    script.Destroy();
                    SetHpText();
                    StartCoroutine(TimeLeft(60));
                    StartChamber();
                }
            }
        }

        if (doorTrigger == null)
        {
            SetHpText();
        }

        if (activeArea != null)
        {
            invisibleTriggerController script =
                (invisibleTriggerController)
                activeArea.GetComponent(typeof (invisibleTriggerController));
            if (script.bossLeftArea)
            {
                EndChamber();
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && playerOnChamber)
        {
            StartChamber();
        }
    }

    void StartChamber()
    {
        hideRamps();
        hideLetter (letter);
        spawnDoor (doorToSpawn);
        destroyChamberObjects();
        player.transform.position =
            new Vector3(-8.54803944f, 5.0686388f, 7.92297173f);
        instantiateEnemies();
    }

    void SetHpText()
    {
        if (player != null)
        {
            playerController script =
                (playerController)
                player.GetComponent(typeof (playerController));
            if (script != null)
            {
                playerHp = script.getHp();
            }
        }

        hpLeftText.text = playerHp.ToString() + "hp left";
        if (playerHp <= 0)
        {
            gameOverText.text = "GAME OVER";

            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }

    public IEnumerator TimeLeft(float time)
    {
        timeLeft = time;
        while (timeLeft > -1)
        {
            remainingTimeText.text = timeLeft + "s left";
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
        gameOverText.text = "GAME OVER";

        yield return new WaitForSeconds(.4f);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    void instantiateEnemies()
    {
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(-0.439999998f, 8.97999954f, 13.4399996f),
            Quaternion.identity));
    }

    void spawnDoor(GameObject door)
    {
        if (door != null)
        {
            spawnableDoorController script =
                (spawnableDoorController)
                door.GetComponent(typeof (spawnableDoorController));

            if (script != null)
            {
                script.enableColliderAndMesh();
            }
        }
    }

    void deSpawnDoor(GameObject door)
    {
        if (door != null)
        {
            spawnableDoorController script =
                (spawnableDoorController)
                door.GetComponent(typeof (spawnableDoorController));

            if (script != null)
            {
                script.disableColliderAndMesh();
            }
        }
    }

    void destroyChamberObjects()
    {
        foreach (GameObject item in chamberObjects)
        {
            Destroy (item);
        }
    }

    void revealLetter(GameObject letter)
    {
        if (letter != null)
        {
            letterController script =
                (letterController)
                letter.GetComponent(typeof (letterController));
            if (script != null)
            {
                script.activate();
            }
        }
    }

    void hideLetter(GameObject letter)
    {
        if (letter != null)
        {
            letterController script =
                (letterController)
                letter.GetComponent(typeof (letterController));
            if (script != null)
            {
                script.deActivate();
            }
        }
    }

    void hideRamps()
    {
        GameObject[] ramps;
        ramps = GameObject.FindGameObjectsWithTag("Ramp");
        foreach (GameObject item in ramps)
        {
            if (item != null)
            {
                meshRendererHandler meshScript =
                    (meshRendererHandler)
                    item.GetComponent(typeof (meshRendererHandler));

                colliderHandler colScript =
                    (colliderHandler)
                    item.GetComponent(typeof (colliderHandler));
                if (meshScript != null && colScript != null)
                {
                    meshScript.disableMesh();
                    colScript.disableColilider();
                }
            }
        }
    }

    void destroyRamps()
    {
        GameObject[] ramps;
        ramps = GameObject.FindGameObjectsWithTag("Ramp");
        foreach (GameObject item in ramps)
        {
            Destroy (item);
        }
    }

    void EndChamber()
    {
        destroyChamberObjects();
        destroyRamps();
        revealLetter (letter);
        deSpawnDoor (doorToSpawn);
        deSpawnDoor(nextDoorChamber);
        Destroy (remainingTimeText);
        Destroy (hpLeftText);
        Destroy (gameObject);
    }
}
