using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class firstChamberManager : MonoBehaviour
{
    private bool playerOnChamber;

    public GameObject doorTrigger;

    public GameObject deathTrigger;

    public GameObject player;

    public GameObject boss;

    public GameObject booster;

    public GameObject doorToSpawn;

    public List<GameObject> nextDoorChamber = new List<GameObject>();

    public GameObject letter;

    public GameObject hiddenUntilEnd;

    private List<GameObject> chamberObjects = new List<GameObject>();

    public TMPro.TMP_Text remainingTimeText;

    public TMPro.TMP_Text gameOverText;

    float timeLeft;

    void Start()
    {
        playerOnChamber = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (doorTrigger != null)
        {
            invisibleTriggerController script =
                (invisibleTriggerController)
                doorTrigger.GetComponent(typeof (invisibleTriggerController));

            if (script != null)
            {
                if (script.triggered)
                {
                    playerOnChamber = true;
                    script.Destroy();
                    StartCoroutine(TimeLeft(60));
                    StartChamber();
                }
            }
        }

        if (deathTrigger != null)
        {
            invisibleTriggerController script =
                (invisibleTriggerController)
                deathTrigger.GetComponent(typeof (invisibleTriggerController));

            if (script != null)
            {
                if (script.triggered)
                {
                    playerOnChamber = true;
                    script.triggered = false;
                    StartChamber();
                }
            }
        }

        if (letter != null)
        {
            letterController script =
                (letterController)
                letter.GetComponent(typeof (letterController));
            if (!script.isActive)
            {
                EndChamber();
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && playerOnChamber)
        {
            StartChamber();
        }
    }

    void StartChamber()
    {
        // if (player != null)
        // {
        //     playerController script =
        //         (playerController)
        //         player.GetComponent(typeof (playerController));
        //     if (script != null)
        //     {
        //         if (!script.needsToDebuff)
        //         {
        //             script.needsToDebuff = true;
        //         }
        //     }
        // }
        spawnDoor (doorToSpawn);
        destroyChamberObjects();
        player.transform.position =
            new Vector3(6.31800079f, 0.575802028f, 9.55000019f);
        chamberObjects
            .Add(Instantiate(booster,
            new Vector3(8.65999985f, 0.560000002f, 11.0270004f),
            Quaternion.identity));
        instantiateEnemies();
    }

    public IEnumerator TimeLeft(float time)
    {
        timeLeft = time;
        while (timeLeft > -1)
        {
            remainingTimeText.text = timeLeft + "s left";
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
        gameOverText.text = "GAME OVER";

        yield return new WaitForSeconds(.4f);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    void instantiateEnemies()
    {
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(5, 0.493392318f, 12),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(6, 0.493392318f, 13),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(7, 0.493392318f, 14),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(8, 0.493392318f, 15),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(9, 0.493392318f, 16),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(9, 0.493392318f, 7f),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(10, 0.493392318f, 7.5f),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(11, 0.493392318f, 8.3f),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(12, 0.493392318f, 9.2f),
            Quaternion.identity));
        chamberObjects
            .Add(Instantiate(boss,
            new Vector3(13, 0.493392318f, 10.1f),
            Quaternion.identity));
    }

    void spawnDoor(GameObject door)
    {
        if (door != null)
        {
            spawnableDoorController script =
                (spawnableDoorController)
                door.GetComponent(typeof (spawnableDoorController));

            if (script != null)
            {
                script.enableColliderAndMesh();
            }
        }
    }

    void deSpawnDoor(GameObject door)
    {
        if (door != null)
        {
            spawnableDoorController script =
                (spawnableDoorController)
                door.GetComponent(typeof (spawnableDoorController));

            if (script != null)
            {
                script.disableColliderAndMesh();
            }
        }
    }

    void spawnBridge()
    {
        if (hiddenUntilEnd != null)
        {
            hiddenGroupEnabler script =
                (hiddenGroupEnabler)
                hiddenUntilEnd.GetComponent(typeof (hiddenGroupEnabler));

            if (script != null)
            {
                script.ToggleVisibility();
            }
        }
    }

    void destroyChamberObjects()
    {
        foreach (GameObject item in chamberObjects)
        {
            Destroy (item);
        }
    }

    void EndChamber()
    {
        destroyChamberObjects();
        deSpawnDoor (doorToSpawn);
        deSpawnDoor (nextDoorChamber[0]);
        deSpawnDoor (nextDoorChamber[1]);
        spawnBridge();
        Destroy (remainingTimeText);
        Destroy (gameObject);
    }
}
