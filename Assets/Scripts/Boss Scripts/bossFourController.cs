using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossFourController : MonoBehaviour
{
    private GameObject player;

    private Rigidbody rb;

    public int attackStep;

    private Quaternion initialRot;

    float timeLeft;

    void Start()
    {
        //Getters
        player = GameObject.Find("Player");
        rb = GetComponent<Rigidbody>();

        //Initialize
        attackStep = 0;
        initialRot = transform.rotation;
    }

    void Update()
    {
        //Checks attackStep to know when to start attacking
        if (attackStep == 0)
        {
            attack();
        }
    }

    private void attack()
    {
        attackStep++;

        //Waits 3s to attack
        StartCoroutine(nextStep(3));
    }

    public IEnumerator nextStep(float time)
    {
        timeLeft = time;
        while (timeLeft > 0)
        {
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }

        //Faces player
        transform.LookAt(player.transform);

        //Gets impulse
        rb.AddForce(transform.forward * 25f, ForceMode.Impulse);

        //Stops facing player
        transform.rotation = initialRot;

        //Waits 3s again but this time changing the attackStep, so it restarts
        StartCoroutine(waitForAttack(3));
    }

    public IEnumerator waitForAttack(float time)
    {
        timeLeft = time;
        while (timeLeft > 0)
        {
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }

        //Modifies attackStep to read in update
        attackStep = 0;
    }
}
