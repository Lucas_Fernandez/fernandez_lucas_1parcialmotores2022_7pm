using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossTwoController : MonoBehaviour
{
    public float speed = 5;

    private Transform rightTriggerPosition;

    private Transform leftTriggerPosition;

    private GameObject rightTrigger;

    private GameObject leftTrigger;

    public GameObject projectile;

    public bool hittedRight;

    /*The implemented mechanic of moving between 2 points in space 
        should be in a different script*/
        
    private void Awake()
    {
        /*Gets right and left triggers in scene 
            (Should probably change name of trigger to something more representative)*/
        rightTrigger = GameObject.Find("rightTrigger");
        rightTriggerPosition = rightTrigger.transform;

        leftTrigger = GameObject.Find("leftTrigger");
        leftTriggerPosition = leftTrigger.transform;

        //Initialize
        hittedRight = true;
    }

    void Update()
    {
        //If the gameObject hitted right trigger, it moves to the left trigger
        if (hittedRight)
        {
            transform.position =
                Vector3
                    .MoveTowards(transform.position,
                    leftTriggerPosition.position,
                    speed * Time.deltaTime);
        }
        else
        //else it moves towards the right trigger
        {
            transform.position =
                Vector3
                    .MoveTowards(transform.position,
                    rightTriggerPosition.position,
                    speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        /*OnTrigger with a "Position" it changes the bool value 
            of hitted to create the loop of motion */
        if (other.gameObject.CompareTag("Position") == true)
        {
            hittedRight = !hittedRight;
        }

        //OnTrigger with a DoorPlate it spawns a projectile (Barrel)
        if (other.gameObject.CompareTag("DoorPlate") == true)
        {
            GameObject pro;
            pro =
                Instantiate(projectile,
                other.gameObject.transform.position,
                Quaternion.identity);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 1, ForceMode.Impulse);
            Destroy(pro, 10);
        }
    }
}
