using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossFiveButtonController : MonoBehaviour
{
    public bool hitted;

    void Start()
    {
        hitted = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    //Sets if the button has been hit
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Projectile"))
        {
            hitted = true;
            gameObject.SetActive(false);
        }
    }
}
