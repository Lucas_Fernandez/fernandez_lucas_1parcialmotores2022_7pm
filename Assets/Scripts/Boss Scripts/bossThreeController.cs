using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossThreeController : MonoBehaviour
{
    public GameObject minion;

    private int positionCount;

    public bool isDestroyed;

    public List<GameObject> minionsSpawned = new List<GameObject>();

    void Start()
    {
        //Initialize
        positionCount = 0;
        isDestroyed = false;
    }

    void Update()
    {
    }

    private void OnCollisionEnter(Collision col)
    {
        /* OnCollision with a Pedestal, evaluate how many times has changed the position 
            Initiate "Minions" at a given position, adding them to the List */
        if (col.gameObject.CompareTag("Pedestal"))
        {
            if (positionCount == 0)
            {
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-12.3990002f, 1.14999998f, -26.1809998f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-12.3990002f, 1.14999998f, -26.1809998f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-12.3990002f, 1.14999998f, -26.1809998f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-12.3990002f, 1.14999998f, -26.1809998f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-12.3990002f, 1.14999998f, -26.1809998f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-12.3990002f, 1.14999998f, -26.1809998f),
                    Quaternion.identity));
            }

            if (positionCount == 1)
            {
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-17.4300003f, 1.10030377f, -15.0699997f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-17.4300003f, 1.10030377f, -15.0699997f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-17.4300003f, 1.10030377f, -15.0699997f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-17.4300003f, 1.10030377f, -15.0699997f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-17.4300003f, 1.10030377f, -15.0699997f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-17.4300003f, 1.10030377f, -15.0699997f),
                    Quaternion.identity));
            }

            if (positionCount == 2)
            {
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-0.790002406f, 1.10032475f, -22.1200008f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-0.790002406f, 1.10032475f, -22.1200008f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-0.790002406f, 1.10032475f, -22.1200008f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-0.790002406f, 1.10032475f, -22.1200008f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-0.790002406f, 1.10032475f, -22.1200008f),
                    Quaternion.identity));
                minionsSpawned
                    .Add(Instantiate(minion,
                    new Vector3(-0.790002406f, 1.10032475f, -22.1200008f),
                    Quaternion.identity));
            }
        }

        /* OnCollision with a Projectile, change position 2 times to location
            depending on many times the position has changed, 
                third time hitted is sent to "DeathZone" */
        if (col.gameObject.CompareTag("Projectile"))
        {
            if (positionCount == 0)
            {
                changePosition(new Vector3(-17.5900002f, 12, -15.04f));
            }
            else if (positionCount == 1)
            {
                changePosition(new Vector3(-0.660000026f, 12, -21.9200001f));
            }
            else if (positionCount == 2)
            {
                //Should destroy gameObject but crashes
                changePosition(new Vector3(-100, -100, -100)); 
            }
        }
    }

    /* Method for relocating, if the positionCount gets to 3 
        it sets the isDestroyed bool to true 
            to handle the rest of the chamber in the chamberManager */
    private void changePosition(Vector3 pos)
    {
        transform.position = pos;
        positionCount++;
        if (positionCount == 3)
        {
            isDestroyed = true;
        }
    }
}
