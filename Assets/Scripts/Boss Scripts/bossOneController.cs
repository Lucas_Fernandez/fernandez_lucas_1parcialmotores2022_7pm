using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossOneController : MonoBehaviour
{
    private GameObject player;

    public int speed;

    void Start()
    {
        //Getters
        player = GameObject.Find("Player");
    }

    void Update()
    {   
        //Faces player
        transform.LookAt(player.transform);

        //Moves
        transform.Translate(speed * Vector3.forward * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        //OnTrigger with a "DeathField" it gets destroyed
        if (other.gameObject.CompareTag("DeathField") == true)
        {
            Destroy (gameObject);
        }
    }
}
