using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossFiveController : MonoBehaviour
{
    public float speed;

    private List<GameObject> buttons = new List<GameObject>();

    void Start()
    {
        //Gets all buttons in bossObject
        foreach (Transform child in transform)
        {
            buttons.Add(child.gameObject);
        }
    }

    void Update()
    {
        //Moves forward
        transform.Translate(speed * -Vector3.forward * Time.deltaTime);

        //Checks if there are buttons on the boss
        if (buttons.Count != 0)
        {
            //Evaluates if the buttons are hitted
            foreach (GameObject button in buttons)
            {
                if (hitted(button))
                {
                    //Removes button and add speed
                    buttons.Remove (button);
                    speed++;
                }
            }
        }

        //Checks if buttons is empty
        if (buttons.Count == 0)
        {
            //Sends it to deathZone (Should destroyGameObject but it crashes)
            transform.position = new Vector3(-100, -100, -100);
        }
    }

    //Uses buttonScript to see if the button has been hitted
    private bool hitted(GameObject button)
    {
        if (button != null)
        {
            bossFiveButtonController buttonScript =
                (bossFiveButtonController)
                button.GetComponent(typeof (bossFiveButtonController));
            if (buttonScript != null)
            {
                return buttonScript.hitted;
            }
        }
        return false;
    }
}
