using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hiddenGroupEnabler : MonoBehaviour
{
    //Ugly script that disables mesh and collider of a group of GameObjects
    void Update()
    {
    }

    public void ToggleVisibility()
    {
        var collidersObj = gameObject.GetComponentsInChildren<Collider>();
        var meshRenderObj = gameObject.GetComponentsInChildren<MeshRenderer>();

        for (var index = 0; index < meshRenderObj.Length; index++)
        {
            var meshRendererItem = meshRenderObj[index];
            meshRendererItem.enabled = true;
        }

        for (var index = 0; index < collidersObj.Length; index++)
        {
            var colliderItem = collidersObj[index];
            colliderItem.enabled = true;
        }
    }
}
