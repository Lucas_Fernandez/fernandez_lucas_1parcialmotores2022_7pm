using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colliderHandler : MonoBehaviour
{
    //Gets the collider and allows another scripts to enable/disable
    Collider col;

    void Start()
    {
        col = GetComponent<Collider>();
    }

    void Update()
    {
    }

    public void enableColilider()
    {
        col.enabled = true;
    }

    public void disableColilider()
    {
        col.enabled = false;
    }

    public bool coliderEnabled()
    {
        return col.enabled;
    }
}
