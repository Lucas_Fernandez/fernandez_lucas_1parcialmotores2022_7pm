using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meshRendererHandler : MonoBehaviour
{
    //Gets the meshRenderer and allows another scripts to enable/disable
    private MeshRenderer rend;

    void Start()
    {
        rend = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void enableMesh()
    {
        rend.enabled = true;
    }

    public void disableMesh()
    {
        rend.enabled = false;
    }

    public bool rendererEnabled(){
        return rend.enabled;
    }
}
