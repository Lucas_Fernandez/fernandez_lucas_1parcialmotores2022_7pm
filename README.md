# Fernandez_Lucas_1ParcialMotores2022_7pm



## Parcial 01 Motores Graficos 2022, Turno Noche

Single scene with 5 levels/chambers, every chamber has a manager that handles what happens inside the mini-level.
Getting the collectables in the chambers allows the player to access the others.
Getting all the collectables is the win condition.

***/Clarification

All chamberManagers are similar due to the need of making similar mechanichs inside every chamber 
(such as resetting with the "R" key or ending the level when a condition is met)
but have some differences between them
For example, in chambers 3 and 5 the player is allowed to use a Gun, in chambers 2 to 5 hp is displayed
because enemies cause damage (different from chamber 1).
 
There should be a single chamberManager script but I didn't have the time to consolidate all 5 in just 1.


Here is the basic functioning of the chamberManager Scripts used:

_onStart asumes the player is outside the chamber
_onUpdate the first method checks if the trigger placed in the entrance of the chamber has been hit
when the trigger is hit, the StartChamber method is invoked. This method invokes all methods 
that constitute the start of the chamber. When pressing R to reset the chamber, this method 
will be called again if the player is inside the chamber.
_additionally OnUpdate will check for and End Condition, it varies depending the chamber. 
When the condition is met, the EndChamber method will be invoked, calling all the methods needed to
end the challenge (like destroying all gameObjects natives to the chamber and revealling the won collectable)
and enable the consequent chamber.
